Source: virify
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Homepage: https://github.com/EBI-Metagenomics/emg-viral-pipeline
Vcs-Browser: https://salsa.debian.org/med-team/virify
Vcs-Git: https://salsa.debian.org/med-team/virify.git
Rules-Requires-Root: no

Package: virify-cwl
Architecture: all
Depends: ${misc:Depends},
         cwltool|toil,
         virify
Description: CWL-implementation of VIRify pipeline
 This pipeline is run when there is an interest in finding viruses in
 a sample but one does not really know what to look for. The sequences
 provides all RNA sequences or all DNA sequences that it could find,
 likely somehow helped biochemically to increase chances to find the
 viral DNA/RNA, not the host's DNA/RNA, but any such enrichment is
 not ultimately required. Just sequence more, aka "deeper". Once this
 analysis is run, one will likely define PCR tests for the virus(es)
 identified.
 .
 VIRify is a recently developed pipeline for the detection, annotation,
 and taxonomic classification of viral contigs in metagenomic and
 metatranscriptomic assemblies. The pipeline is part of the repertoire of
 analysis services offered by the European Bioinformatics Institute.
 .
 VIRify’s taxonomic classification
 relies on the detection of taxon-specific profile hidden Markov models
 (HMMs), built upon a set of 22,014 orthologous protein domains and
 referred to as ViPhOGs.
 ,
 This package provides the CWL-implementaion of that pipeline.

Package: virify-nextflow
Architecture: all
Depends: ${misc:Depends},
         nextflow,
         virify
Description: nextflow-implementation of VIRify pipeline
 This pipeline is run when there is an interest in finding viruses in
 a sample but one does not really know what to look for. The sequences
 provides all RNA sequences or all DNA sequences that it could find,
 likely somehow helped biochemically to increase chances to find the
 viral DNA/RNA, not the host's DNA/RNA, but any such enrichment is
 not ultimately required. Just sequence more, aka "deeper". Once this
 analysis is run, one will likely define PCR tests for the virus(es)
 identified.
 .
 VIRify is a recently developed pipeline for the detection, annotation,
 and taxonomic classification of viral contigs in metagenomic and
 metatranscriptomic assemblies. The pipeline is part of the repertoire of
 analysis services offered by the European Bioinformatics Institute.
 .
 VIRify’s taxonomic classification
 relies on the detection of taxon-specific profile hidden Markov models
 (HMMs), built upon a set of 22,014 orthologous protein domains and
 referred to as ViPhOGs.
 ,
 This package provides the Mextflow-implementaion of that pipeline.

Package: virify
Architecture: all
Depends: ${misc:Depends},
         python3,
         ncbi-blast+, 
         fastqc,
         krona,
         multiqc,
         prodigal,
         r-cran-optparse,
         r-cran-gggenes,
         r-cran-rcolorbrewer,
         r-cran-chromomap,
         r-cran-ggplot2,
         r-cran-plotly,
         libjs-sankeyd3,
         virfinder,
         bioruby,
         fastp,
         hmmer,
         mashmap,
         phanotate,
         pprmeta,
         spades,
         virsorter
Description: finds viruses in metagenomic and metatranscriptomic data
 This pipeline is run when there is an interest in finding viruses in
 a sample but one does not really know what to look for. The sequences
 provides all RNA sequences or all DNA sequences that it could find,
 likely somehow helped biochemically to increase chances to find the
 viral DNA/RNA, not the host's DNA/RNA, but any such enrichment is
 not ultimately required. Just sequence more, aka "deeper". Once this
 analysis is run, one will likely define PCR tests for the virus(es)
 identified.
 .
 VIRify is a recently developed pipeline for the detection, annotation,
 and taxonomic classification of viral contigs in metagenomic and
 metatranscriptomic assemblies. The pipeline is part of the repertoire of
 analysis services offered by the European Bioinformatics Institute.
 .
 VIRify’s taxonomic classification
 relies on the detection of taxon-specific profile hidden Markov models
 (HMMs), built upon a set of 22,014 orthologous protein domains and
 referred to as ViPhOGs.
 ,
 The pipeline is implemented and available in CWL and Nextflow. Please
 see respective packages.
